import axios from 'axios'
import {Sequelize} from 'sequelize'
import config from "config";

const sequelize = new Sequelize(config.get('pg_name'), config.get('pg_username'), config.get('pg_password'), {
    host: config.get('pg_host'),
    dialect: 'postgres'
});

const Projects = sequelize.define("projects", {
    id: {
        type: Sequelize.STRING,
        primaryKey: true,
        allowNull: false
    },
    name: {
        type: Sequelize.STRING,
        allowNull: false
    },
    short_name: {
        type: Sequelize.STRING,
        allowNull: false
    },
    trial407: {
        type: Sequelize.STRING,
        allowNull: false
    },
    skills: {
        type: Sequelize.JSON,
        allowNull: false
    },
    payments: {
        type: Sequelize.JSON,
        allowNull: false
    }
});


export const setProject = async (req, res) => {

    await Projects.sync();

    let configConnect = {
        method: 'get',
        url: config.get('api_host') + '/api/admin/projects?fields=id,name,shortName&$skip=0&$top=1000',
        data: {
            orders: [
                {
                    order_sum: '1990',
                    'sender-zip': '190000',
                    'client-zip': '143408',
                    name: 'тест',
                    sender: 'sender',
                    'sender-addr': 'sender-addr',
                    'client-addr': 'client-addr'
                }
            ]
        },
        headers: {
            'Authorization': config.get('token')
        }
    };

    axios(configConnect)
        .then(function (response) {
            for (let d of response.data) {
                Projects.create({
                    id: d.id,
                    name: d.name,
                    short_name: d.shortName,
                    trial407: ""
                }).then(res => {
                    console.log(res);
                }).catch(err => console.log(err));
            }
            res.json('success');
        })
        .catch(function (error) {
            res.json(error);
        });

}

export const getProject = async (req, res) => {

    await Projects.sync();
    Projects.findAll().then(result => {
        return res.status(200).json({
            result,
        });
    }).catch(err => console.log(err));



}