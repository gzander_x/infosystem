import axios from 'axios'
import {Sequelize} from 'sequelize'
import config from "config";

/*app.use((required, response, next) => {
  if (required.method == 'OPTIONS') {
    response.status(200).send({})
  } else {
    next()
  }
})*/



const sequelize = new Sequelize(config.get('pg_name'), config.get('pg_username'), config.get('pg_password'), {
  host: config.get('pg_host'),
  dialect: 'postgres'
});

export const migUsers = async (req, res) => {
  const Projects = sequelize.define("projects", {
    id: {
      type: Sequelize.STRING,
      primaryKey: true,
      allowNull: false
    },
    name: {
      type: Sequelize.STRING,
      allowNull: false
    },
    short_name: {
      type: Sequelize.STRING,
      allowNull: false
    },
    trial407: {
      type: Sequelize.STRING,
      allowNull: false
    }
  });

  await Projects.sync();

  let configConnect = {
    method: 'get',
    url: config.get('api_host') + '/hub/api/rest/projectteams/?$top=-1&fields=id,project(name),name,users(name)',
    headers: {
      'Authorization': config.get('token')
    }
  };
// требуется id тут его нет
  axios(configConnect)
      .then(function (response) {
        for (let p of response.data.projectteams) {
          if (p.users) {
            for (let u of p.users) {
              Projects.create({
                project: p.project.name,
                name: u.name,
              }).then(res => {
                console.log(res);
              }).catch(err => console.log(err));
            }
          }
        }
        res.send('End migration')
      })
      .catch(function (error) {
        res.json(error);
      });

}

export const migTasks = async (req, res) => {
  const Tasks = sequelize.define("tasks", {
    id: {
      type: Sequelize.INTEGER,
      autoIncrement: true,
      primaryKey: true,
      allowNull: false
    },
    date: {
      type: Sequelize.BIGINT(),
      allowNull: false
    },
    task_id: {
      type: Sequelize.STRING,
      allowNull: false
    },
    minutes: {
      type: Sequelize.INTEGER,
      allowNull: false
    },
    name: {
      type: Sequelize.STRING,
      allowNull: false
    },
    trial407: {
      type: Sequelize.STRING,
      allowNull: true
    },
  });

  const Task_spent_time = sequelize.define("task_spent_time", {
    id: {
      type: Sequelize.INTEGER,
      autoIncrement: true,
      primaryKey: true,
      allowNull: false
    },
    date: {
      type: Sequelize.BIGINT(),
      allowNull: false
    },
    task_id: {
      type: Sequelize.STRING,
      allowNull: false
    },
    minutes: {
      type: Sequelize.INTEGER,
      allowNull: false
    },
    name: {
      type: Sequelize.STRING,
      allowNull: false
    },
    trial407: {
      type: Sequelize.STRING,
      allowNull: true
    },
  });

  await Task_spent_time.sync();
  await Tasks.sync();

  let configConnect = {
    method: 'get',
    url: config.get('api_host') + '/api/issues?skip=0&top=1000&fields=idReadable,id,created,summary,description,project(id),customFields(name,value(name,localizedName,minutes))&customFields=estimation&customFields=Subsystem&customFields=state&&customFields=Type',
    headers: {
      'Authorization': config.get('token')
    }
  };

  axios(configConnect)
      .then(function (response) {
        let prom = []

        for (let d of response.data) {
          let st = new Date(d.created),
              unix = moment(st).unix()


          let subsystem = null,
              estimation = 0,
              state = null,
              type = null

          for (let c of d.customFields) {
            if (c.name == 'Subsystem' && c.value) {
              subsystem = c.value.name
            }
            if (c.name == 'Estimation' && c.value) {
              estimation = c.value.minutes
            }
            if (c.name == 'Type' && c.value) {
              type = c.value.localizedName
                  ? c.value.localizedName.toLowerCase()
                  : c.value.name.toLowerCase()
            }
            if (c.name == 'State') {
              if (c.value.localizedName != null) {
                state = c.value.localizedName.toLowerCase()
              } else {
                state = c.value.name.toLowerCase()
              }
            }
          }

          Tasks.create({
            task_id: d.id,
            project_id: d.project.id,
            name: d.summary,
            id_readable: d.idReadable,
            description: d.description,
            estimation: estimation,
            subsystem: subsystem,
            state: state,
            type: type,
            created: unix
          }).then(res => {
            console.log(res);
          }).catch(err => console.log(err));


          prom.push(
              axios({
                method: 'get',
                url: config.get('api_host') + `/api/issues/${d.id}/timeTracking?fields=workItems(created,duration(minutes),creator(name))`,
                // responseType: 'arraybuffer',
                headers: {
                  'Content-Type': 'application/json',
                  'Authorization': config.get('token')
                }
              })
          )
        }

        Promise.all(prom).then(r => {

          for (let o of r) {
            let task_id = o.request.path.split('/')[3]
            if (o.data.workItems.length > 0) {
              for (let w of o.data.workItems) {
                let st = new Date(w.created),
                    unix = moment(st).unix()

                Task_spent_time.create({
                  task_id: task_id,
                  date: unix,
                  name: d.summary,
                  minutes: w.duration.minutes,
                }).then(res => {
                  console.log(res);
                }).catch(err => console.log(err));

              }
            }
          }
          res.send('End migration')
        })
      })
      .catch(function (error) {
        res.json(error);
      });

}




/*
app.get('/mig_projects', (req, res) => {
  axios({
    method: 'get',
    url:
      'http://94.127.67.113:8070/api/admin/projects?fields=id,name,shortName&$skip=0&$top=1000',
    // responseType: 'arraybuffer',
    headers: {
      'Content-Type': 'application/json',
      Authorization: 'Bearer ' + key
    }
  }).then(function (response) {
    let addValues = [],
      addInsertColumn = []

    for (let d of response.data) {
      addValues.push(d.id)
      addValues.push(d.name)
      addValues.push(d.shortName)
      addInsertColumn.push('(?,?,?)')
    }
    q = `insert INTO projects (id, name, short_name) values ${addInsertColumn.join(
      ','
    )} ON CONFLICT(id) DO UPDATE SET name = name`
    db.insert(q, addValues, function (err, data) {
      if (err) {
        res.status(500).send(data)
      } else {
        res.send('End migration')
      }
    })
  })
})
*/

export  const planFact = async (req, res) => {


  //let  period = {name: 4, value: [1610565924, 1614546000]}
  let filterValue = []
  if (req.body.period) {
    if (
        parseInt(req.body.period.value[0]) &&
        parseInt(req.body.period.value[1])
    ) {
      filterValue.push('t.created>=' + req.body.period.value[0])
      filterValue.push('t.created<=' + req.body.period.value[1])
    }
  }

  const [results, metadata] =  await sequelize.query(
      "select * from projects"
  )

  const [results2, metadata2] =  await sequelize.query(
      `select sum(tst.minutes) as minutes,sum(t.estimation) as estimation, t.project_id from 
      task_spent_time as tst
      left join tasks as t on t.task_id = tst.task_id
        ${filterValue.length > 0 ? ' where ' + filterValue.join(' and ') : ''}
      group by t.project_id  
      `
  )

/*  if (period) {
    if (
        parseInt(period.value[0]) &&
        parseInt(period.value[1])
    ) {
      filterValue.push('t.created>=' + period.value[0])
      filterValue.push('t.created<=' + period.value[1])
    }
  }*/


  Promise.all([
    new Promise(function (resolve, reject) {
      if (results) {
        resolve(results)
      }else{
        reject(results)
      }

    }),
    new Promise(function (resolve, reject) {
      if (!results2) {
        reject(results2)
      }
      let st = {}
      for (let d of results2) {
        st[d.project_id] = {
          spent_time: d.minutes,
          estimation: d.estimation
        }
      }
      resolve(st)

    })
  ])
      .then(([projectL, spentTime]) => {

        let project = {}

        for (let p of projectL) {
          if (spentTime[p.id]) {
            p.spent_time = spentTime[p.id].spent_time
            p.estimation = spentTime[p.id].estimation
            project[p.id] = p
          }
        }

        res.send(project)
      })
      .catch(err => {
        console.log(err)
        res.status(500).send(err)
      })
}


export  const users = async (req, res) => {

 // let  period = {name: 1, value: [1610615883, 1614546000]}
  let filterValue = []
  if (req.body.period) {
    if (
      parseInt(req.body.period.value[0]) &&
      parseInt(req.body.period.value[1])
    ) {
      filterValue.push('t.created>=' + req.body.period.value[0])
      filterValue.push('t.created<=' + req.body.period.value[1])
    }
  }
/*  if (period) {
    if (
        parseInt(period.value[0]) &&
        parseInt(period.value[1])
    ) {
      filterValue.push('t.created>=' + period.value[0])
      filterValue.push('t.created<=' + period.value[1])
    }
  }*/
  try {
/*    const [results, metadata2] =  await sequelize.query(
        `select tst.name, t.state, t.estimation, tst.minutes, t.state, t.type from task_spent_time tst
  left join tasks t on tst.task_id = t.task_id 
`
    )*/

    const [results, metadata2] =  await sequelize.query(
        `select tst.name, t.state, t.estimation, tst.minutes, t.state, t.type from task_spent_time tst
  left join tasks t on tst.task_id = t.task_id
  ${filterValue.length > 0 ? ' where ' + filterValue.join(' and ') : ''}`
    )


    let users = {},
        closeState = ['решена', 'закрыта']
    for (let d of results) {
      if (!users[d.name]) {
        users[d.name] = {
          estimation: 0,
          spent_time: 0,
          complete_task_count: 0,
          open_task_count: 0,
          count_task: 0,
          count_task_error: 0
        }
      }

      if (closeState.includes(d.state)) {
        users[d.name].spent_time += d.minutes
      }
      if (closeState.includes(d.state)) {
        users[d.name].spent_time += d.minutes
        ++users[d.name].complete_task_count
      } else {
        users[d.name].estimation += d.estimation
        ++users[d.name].open_task_count
      }
      if (d.type == 'задание') {
        ++users[d.name].count_task
      }
      if (d.type == 'ошибка') {
        ++users[d.name].count_task_error
      }
    }
    res.send(users)

  } catch (error) {
    console.log(error)
  }



}

export  const tasks = async (req, res) => {

  //let  period = {name: 1, value: [1610615883, 1614546000]}
  let filterValue = []
  if (req.body.period) {
    if (
      parseInt(req.body.period.value[0]) &&
      parseInt(req.body.period.value[1])
    ) {
      filterValue.push('t.created>=' + req.body.period.value[0])
      filterValue.push('t.created<=' + req.body.period.value[1])
    }
  }

/*  if (period) {
    if (
        parseInt(period.value[0]) &&
        parseInt(period.value[1])
    ) {
      filterValue.push('t.created>=' + period.value[0])
      filterValue.push('t.created<=' + period.value[1])
    }
  }*/

  const [results, metadata] =  await sequelize.query(
      "select * from projects"
  )

  const [results2, metadata2] =  await sequelize.query(
      `select t.project_id, count(tAll.task_id) as tatal_tasks,count(tCom.task_id) as tasks_complete from 
      tasks as t
      left join tasks as tAll on tAll.task_id = t.task_id
      left join tasks as tCom on tCom.task_id = t.task_id and tCom.state in ('решена','закрыта')
      ${filterValue.length > 0 ? ' where ' + filterValue.join(' and ') : ''}
      group by t.project_id  
      `
  )

  Promise.all([
    new Promise(function (resolve, reject) {
      if (!results) {
        reject(results)
      } else {
        resolve(results)
      }
    }),
    new Promise(function (resolve, reject) {
      if (!results2) {
        reject(results2)
      } else {
        let st = {}
        for (let d of results2) {
          st[d.project_id] = {
            total: d.tatal_tasks,
            complete: d.tasks_complete
          }
        }
        resolve(st)
      }

    })
  ])
    .then(([projectL, countTasks]) => {
      let project = {}
      for (let p of projectL) {
        if (countTasks[p.id]) {
          p.total = countTasks[p.id].total
          p.complete = countTasks[p.id].complete
          project[p.id] = p
        } else {
          p.total = 0
          p.complete = 0
          project[p.id] = p
        }
      }

      res.send(project)
    })
    .catch(err => {
      res.status(500).send(err)
    })
}



export  const planFactByPlatform = async (req, res) => {

  let filterValue = []
 // let  period = {name: 1, value: [1610615883, 1614546000]}

 if (req.body.period) {
    if (
      parseInt(req.body.period.value[0]) &&
      parseInt(req.body.period.value[1])
    ) {
      filterValue.push('t.created>=' + req.body.period.value[0])
      filterValue.push('t.created<=' + req.body.period.value[1])
    }
  }

/*  if (period) {
    if (
        parseInt(period.value[0]) &&
        parseInt(period.value[1])
    ) {
      filterValue.push('t.created>=' + period.value[0])
      filterValue.push('t.created<=' + period.value[1])
    }
  }*/

  const [results, metadata] =  await sequelize.query(
      "select * from projects"
  )


    const [results2, metadata2] =  await sequelize.query(
        `select sum(tst.minutes) as minutes,sum(t.estimation) as estimation,t.subsystem, t.project_id from 
      task_spent_time as tst
      left join tasks as t on t.task_id = tst.task_id
      where lower(t.subsystem) = 'android' ${
            filterValue.length > 0 ? ' and ' + filterValue.join(' and ') : ''
        }
      group by t.project_id  ,t.subsystem
      `
    )


  const [results3, metadata3] =  await sequelize.query(
      `select sum(tst.minutes) as minutes,sum(t.estimation) as estimation,t.subsystem, t.project_id from 
      task_spent_time as tst
      left join tasks as t on t.task_id = tst.task_id
      where lower(t.subsystem) = 'ios' ${
          filterValue.length > 0 ? ' and ' + filterValue.join(' and ') : ''
      }
      group by t.project_id  ,t.subsystem
      `
  )

  const [results4, metadata4] =  await sequelize.query(
      `select sum(tst.minutes) as minutes,sum(t.estimation) as estimation,t.subsystem, t.project_id from 
      task_spent_time as tst
      left join tasks as t on t.task_id = tst.task_id
      where (lower(t.subsystem) != 'android' and lower(t.subsystem) != 'ios')  or t.subsystem ISNULL  ${
          filterValue.length > 0 ? ' and ' + filterValue.join(' and ') : ''
      }
      group by t.project_id  ,t.subsystem
      `
  )

  Promise.all([
    new Promise(function (resolve, reject) {

      if (!results) {
        reject(results)
      }

      let st = {}
      for (let d of results) {
        st[d.id] = {
          name: d.name,
          short_name: d.short_name
        }
      }
      resolve(st)
    }),
    new Promise(function (resolve, reject) {

      if (!results2) {
        reject(results2)
      }

      let st = {}
      for (let d of results2) {
        st[d.project_id] = {
          spent_time: d.minutes,
          subsystem: d.subsystem,
          estimation: d.estimation
        }
      }
      resolve(st)

    }),
    new Promise(function (resolve, reject) {

      if (!results3) {
        reject(results3)
      }

      let st = {}
      for (let d of results3) {
        st[d.project_id] = {
          spent_time: d.minutes,
          subsystem: d.subsystem,
          estimation: d.estimation
        }
      }
      resolve(st)


    }),
    new Promise(function (resolve, reject) {

      if (!results4) {
        reject(results4)
      }

      let st = {}
      for (let d of results4) {
        st[d.project_id] = {
          spent_time: d.minutes,
          subsystem: d.subsystem,
          estimation: d.estimation
        }
      }
      resolve(st)

    })
  ])
    .then(([projectL, spentTimeAndroid, spentTimeIOS, spentTimeOther]) => {
      let project = {}

      for (let pAnd of Object.keys(projectL)) {
        let p = {}

        if (spentTimeAndroid[pAnd]) {
          p = {
            name:
              projectL[pAnd].name +
              ' (' +
              spentTimeAndroid[pAnd].subsystem +
              ')',
            short_name: projectL[pAnd].short_name,
            spent_time: spentTimeAndroid[pAnd].spent_time,
            estimation: spentTimeAndroid[pAnd].estimation,
            type: spentTimeAndroid[pAnd].subsystem
          }
          project[p.name] = p
        }
        if (spentTimeIOS[pAnd]) {
          p = {
            name:
              projectL[pAnd].name + ' (' + spentTimeIOS[pAnd].subsystem + ')',
            short_name: projectL[pAnd].short_name,
            spent_time: spentTimeIOS[pAnd].spent_time,
            estimation: spentTimeIOS[pAnd].estimation,
            type: spentTimeIOS[pAnd].subsystem
          }
          project[p.name] = p
        }
        if (spentTimeOther[pAnd]) {
          p = {
            name:
              projectL[pAnd].name +
              ' (' +
              (spentTimeOther[pAnd].subsystem || 'Other') +
              ')',
            short_name: projectL[pAnd].short_name,
            spent_time: spentTimeOther[pAnd].spent_time,
            estimation: spentTimeOther[pAnd].estimation,
            type: spentTimeOther[pAnd].subsystem
          }
          project[p.name] = p
        }
      }

      res.send(project)
    })
    .catch(err => {
      res.status(500).send(err)
    })
}


export  const tasksByPlatform = async (req, res) => {
  //let  period = {name: 1, value: [1610615883, 1614546000]}
  let filterValue = []
  if (req.body.period) {
    if (
      parseInt(req.body.period.value[0]) &&
      parseInt(req.body.period.value[1])
    ) {
      filterValue.push('t.created>=' + req.body.period.value[0])
      filterValue.push('t.created<=' + req.body.period.value[1])
    }
  }

/*  if (period) {
    if (
        parseInt(period.value[0]) &&
        parseInt(period.value[1])
    ) {
      filterValue.push('t.created>=' + period.value[0])
      filterValue.push('t.created<=' + period.value[1])
    }
  }*/

  const [results, metadata] =  await sequelize.query(
      "select * from projects"
  )

  const [results2, metadata2] =  await sequelize.query(
      `
      select t.project_id,t.subsystem, count(tAll.task_id) as tatal_tasks,count(tCom.task_id) as tasks_complete from 
      tasks as t
      left join tasks as tAll on tAll.task_id = t.task_id
      left join tasks as tCom on tCom.task_id = t.task_id and tCom.state in ('решена','закрыта')
      where lower(t.subsystem) = 'android' ${
          filterValue.length > 0 ? ' and ' + filterValue.join(' and ') : ''
      }
      group by t.project_id`
  )

  const [results3, metadata3] =  await sequelize.query(
      `select t.project_id,t.subsystem, count(tAll.task_id) as tatal_tasks,count(tCom.task_id) as tasks_complete from 
      tasks as t
      left join tasks as tAll on tAll.task_id = t.task_id
      left join tasks as tCom on tCom.task_id = t.task_id and tCom.state in ('решена','закрыта')
      where lower(t.subsystem) = 'ios' ${
          filterValue.length > 0 ? ' and ' + filterValue.join(' and ') : ''
      }
      group by t.project_id 
      `
  )

  const [results4, metadata4] =  await sequelize.query(
      `
      select t.project_id,t.subsystem, count(tAll.task_id) as tatal_tasks,count(tCom.task_id) as tasks_complete from 
      tasks as t
      left join tasks as tAll on tAll.task_id = t.task_id
      left join tasks as tCom on tCom.task_id = t.task_id and tCom.state in ('решена','закрыта')
      where (lower(t.subsystem) != 'android' and lower(t.subsystem) != 'ios')  or t.subsystem ISNULL ${
          filterValue.length > 0 ? ' and ' + filterValue.join(' and ') : ''
      }
      group by t.project_id,t.subsystem
      `
  )

  Promise.all([
    new Promise(function (resolve, reject) {

      if (!results) {
        reject(results)
      }

      let st = {}
      for (let d of results) {
        st[d.id] = {
          name: d.name,
          short_name: d.short_name
        }
      }
      resolve(st)

    }),
    new Promise(function (resolve, reject) {

      if (!results2) {
        reject(results2)
      }

      let st = {}
      for (let d of results2) {
        st[d.project_id] = {
          total: d.tatal_tasks,
          complete: d.tasks_complete,
          subsystem: d.subsystem
        }
      }
      resolve(st)

    }),
    new Promise(function (resolve, reject) {

      if (!results3) {
        reject(results3)
      }

      let st = {}
      for (let d of results3) {
        st[d.project_id] = {
          total: d.tatal_tasks,
          complete: d.tasks_complete,
          subsystem: d.subsystem
        }
      }
      resolve(st)

    }),
    new Promise(function (resolve, reject) {

      if (!results4) {
        reject(results4)
      }

      let st = {}
      for (let d of data) {
        st[d.project_id] = {
          total: d.tatal_tasks,
          complete: d.tasks_complete,
          subsystem: d.subsystem
        }
      }
      resolve(st)

    })
  ])
    .then(([projectL, spentTimeAndroid, spentTimeIOS, spentTimeOther]) => {
      let project = {}

      for (let pAnd of Object.keys(projectL)) {
        let p = {}

        if (spentTimeAndroid[pAnd]) {
          p = {
            name:
              projectL[pAnd].name +
              ' (' +
              spentTimeAndroid[pAnd].subsystem +
              ')',
            short_name: projectL[pAnd].short_name,
            total: spentTimeAndroid[pAnd].total,
            complete: spentTimeAndroid[pAnd].complete,
            type: spentTimeAndroid[pAnd].subsystem
          }

          project[p.name] = p
        }
        if (spentTimeIOS[pAnd]) {
          p = {
            name:
              projectL[pAnd].name + ' (' + spentTimeIOS[pAnd].subsystem + ')',
            short_name: projectL[pAnd].short_name,
            total: spentTimeIOS[pAnd].total,
            complete: spentTimeIOS[pAnd].complete,
            type: spentTimeIOS[pAnd].subsystem
          }
          project[p.name] = p
        }
        if (spentTimeOther[pAnd]) {
          p = {
            name:
              projectL[pAnd].name +
              ' (' +
              (spentTimeOther[pAnd].subsystem || 'Other') +
              ')',
            short_name: projectL[pAnd].short_name,
            total: spentTimeOther[pAnd].total,
            complete: spentTimeOther[pAnd].complete,
            type: spentTimeOther[pAnd].subsystem
          }
          project[p.name] = p
        }
      }
      res.send(project)
    })
    .catch(err => {
      res.status(500).send(err)
    })
}


export  const commitsByProject = async (req, res) => {

  // let  period = {name: 1, value: [1610615883, 1614546000]}
  let filterValue = []
  if (req.body.period) {
    if (
        parseInt(req.body.period.value[0]) &&
        parseInt(req.body.period.value[1])
    ) {
      filterValue.push('t.created>=' + req.body.period.value[0])
      filterValue.push('t.created<=' + req.body.period.value[1])
    }
  }
  /*  if (period) {
      if (
          parseInt(period.value[0]) &&
          parseInt(period.value[1])
      ) {
        filterValue.push('t.created>=' + period.value[0])
        filterValue.push('t.created<=' + period.value[1])
      }
    }*/
  try {
    /*    const [results, metadata2] =  await sequelize.query(
            `select tst.name, t.state, t.estimation, tst.minutes, t.state, t.type from task_spent_time tst
      left join tasks t on tst.task_id = t.task_id
    `
        )*/

    const [results, metadata2] =  await sequelize.query(
        `select tst.hash,tst.href,tst.author,tst.date,tst.date, t.name from commits tst
  left join projects t on tst.project_id = t.id ORDER BY tst.date DESC  LIMIT 20
`
    )

    let commits = []
    for (let d of results) {
      commits.push( {
          name: d.name,
        hash: d.hash,
        href: d.href,
        author: d.author,
        date: d.date,
        })
    }
    res.send(commits)

  } catch (error) {
    console.log(error)
  }



}
export  const commitsByUser = async (req, res) => {

  // let  period = {name: 1, value: [1610615883, 1614546000]}
  let filterValue = []
  if (req.body.period) {
    if (
        parseInt(req.body.period.value[0]) &&
        parseInt(req.body.period.value[1])
    ) {
      filterValue.push('t.created>=' + req.body.period.value[0])
      filterValue.push('t.created<=' + req.body.period.value[1])
    }
  }
  /*  if (period) {
      if (
          parseInt(period.value[0]) &&
          parseInt(period.value[1])
      ) {
        filterValue.push('t.created>=' + period.value[0])
        filterValue.push('t.created<=' + period.value[1])
      }
    }*/
  try {
    /*    const [results, metadata2] =  await sequelize.query(
            `select tst.name, t.state, t.estimation, tst.minutes, t.state, t.type from task_spent_time tst
      left join tasks t on tst.task_id = t.task_id
    `
        )*/

    const [results, metadata2] =  await sequelize.query(
        `select tst.hash,tst.href,u.fio,tst.date,tst.date from commits tst
      left join user_commits uc on uc.name = tst.author 
       left join users u on u.id = uc.user_id 
         ORDER BY tst.date DESC LIMIT 20
`
    )

    let commits = []
    for (let d of results) {
      commits.push( {
        name: d.fio,
        hash: d.hash,
        href: d.href,
        author: d.author,
        date: d.date,
      })
    }

    res.send(commits)

  } catch (error) {
    console.log(error)
  }



}
