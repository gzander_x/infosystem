import axios from 'axios'
import {Sequelize} from 'sequelize'
import config from "config";
import {validateCreateUser,validateUpdateUser} from "../helpers/requestValidator.js"

const sequelize = new Sequelize(config.get('pg_name'), config.get('pg_username'), config.get('pg_password'), {
    host: config.get('pg_host'),
    dialect: 'postgres'
});

const Users = sequelize.define("users", {
    id: {
        type: Sequelize.INTEGER,
        autoIncrement: true,
        primaryKey: true,
        allowNull: false
    },
    fio: {
        type: Sequelize.STRING,
        allowNull: false,
        unique: true
    },
    position: {
        type: Sequelize.STRING,
        allowNull: true
    },
    date_start: {
        type: Sequelize.STRING,
        allowNull: true
    },
    form_cooperation: {
        type: Sequelize.STRING,
        allowNull: true
    },
    requisites: {
        type: Sequelize.JSON,
        allowNull: true
    },
    skills: {
        type: Sequelize.JSON,
        allowNull: true
    },
    projects: {
        type: Sequelize.JSON,
        allowNull: true
    }
});

export const getUsers = async (req, res) => {
    const User = sequelize.define("project_users", {
        id: {
            type: Sequelize.INTEGER,
            autoIncrement: true,
            primaryKey: true,
            allowNull: false
        },
        project: {
            type: Sequelize.STRING,
            allowNull: false
        },
        name: {
            type: Sequelize.STRING,
            allowNull: false
        },
        trial404: {
            type: Sequelize.STRING,
            allowNull: false
        }
    });


    await User.sync({force: true});

    let configConnect = {
        method: 'get',
        url: config.get('api_host') + '/hub/api/rest/projectteams/?$top=-1&fields=id,project(name),name,users(name)',
        headers: {
            'Authorization': config.get('token')
        }
    };

    axios(configConnect)
        .then(function (response) {
            for (let p of response.data.projectteams) {
                if (p.users) {
                    for (let u of p.users) {
                        User.create({
                            project: p.project.name,
                            name: u.name,
                            trial404: ""
                        }).then(res => {
                            console.log(res);
                        }).catch(err => console.log(err));

                    }
                }
            }
            res.json('Success');
        })
        .catch(function (error) {
            res.json(error);
        });

}


export const createUser = async (req, res) => {
    const body = req.body;
    // validation
    const { error } = validateCreateUser(body);
    if(error) return res.status(400).json({
        success: false,
        message: error
    });


    await Users.sync();

    Users.create({
        fio: req.body.fio,
        position: req.body.position,
        date_start: req.body.date_start,
        form_cooperation:req.body.form_cooperation,
        requisites: req.body.requisites,
        skills: req.body.skills,
        projects:req.body.projects,
    }).then(result => {
        return res.status(200).json({
            success: true,
        });
    }).catch(err => console.log(err));

}

export const updateUser = async (req, res) => {
    const body = req.body;
    // validation
    const { error } = validateUpdateUser(body);
    if(error) return res.status(400).json({
        success: false,
        message: error
    });

    await Users.sync();

    await Users.update({
        fio: req.body.fio,
        position: req.body.position,
        date_start: req.body.date_start,
        form_cooperation:req.body.form_cooperation,
        requisites: req.body.requisites,
        skills: req.body.skills,
        projects:req.body.projects,
    }, {
        where: {
            id: req.body.id,
        }
    }).then(result => {
        return res.status(200).json({
            success: true,
        });
    }).catch(err => console.log(err));

}

export const getUser = async (req, res) => {

    await Users.sync();
    Users.findAll().then(result => {
        return res.status(200).json({
            result,
        });
    }).catch(err => console.log(err));

}

export const importUsers = async (req, res) => {


    let configConnect = {
        method: 'get',
        url: config.get('api_host') + '/hub/api/rest/projectteams/?$top=-1&fields=id,project(name),name,users(name)',
        headers: {
            'Authorization': config.get('token')
        }
    };
    await Users.sync();
    axios(configConnect)
        .then(function (response) {
            for (let p of response.data.projectteams) {
                if (p.users) {
                    for (let u of p.users) {

                        Users.create({
                            fio: u.name
                        }).then(result => {

                        }).catch(err => console.log(err));

                    }

                    return res.status(200).json({
                        success: true,
                    });
                }
            }
            res.json('Success');
        })
        .catch(function (error) {
            res.json(error);
        });

}