import {Sequelize} from 'sequelize'
import config from "config";
import {validateCreateSkills, validateUpdateSkills } from "../helpers/requestValidator.js"

const sequelize = new Sequelize(config.get('pg_name'), config.get('pg_username'), config.get('pg_password'), {
    host: config.get('pg_host'),
    dialect: 'postgres'
});

const knowledge = {
    1:"iOS разработка",
    2: "Android",
    3:"Серверная разработка",
    4:"Web-разработка"
}

const Skills = sequelize.define("skills", {
    id: {
        type: Sequelize.INTEGER,
        autoIncrement: true,
        primaryKey: true,
        allowNull: false
    },
    name: {
        type: Sequelize.STRING,
        allowNull: false
    },
    knowledge_id: {
        type: Sequelize.INTEGER,
        allowNull: false
    }
});

export const createSkill = async (req, res) => {
    const body = req.body;
    // validation
    const { error } = validateCreateSkills(body);
    if(error) return res.status(400).json({
        success: false,
        message: error
    });


    await Skills.sync();

    Skills.create({
        name: req.body.name,
        knowledge_id: req.body.knowledge_id,
    }).then(result => {
        return res.status(200).json({
            success: true,
        });
    }).catch(err => console.log(err));

}

export const updateSkill = async (req, res) => {
    const body = req.body;
    // validation
    const { error } = validateUpdateSkills(body);
    if(error) return res.status(400).json({
        success: false,
        message: error
    });

    await Skills.sync();

    await Skills.update({
        name: req.body.name,
        knowledge_id: req.body.knowledge_id,
    }, {
        where: {
            id: req.body.id,
        }
    }).then(result => {
        return res.status(200).json({
            success: true,
        });
    }).catch(err => console.log(err));

}

export const getSkills = async (req, res) => {

    await Skills.sync();
    Skills.findAll().then(result => {
        return res.status(200).json({
            result,
        });
    }).catch(err => console.log(err));

}