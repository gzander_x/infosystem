import {Sequelize} from 'sequelize'
import config from "config";
import {
    validateCreateTimesheetStatus,
    validateUpdateTimesheetStatus,
    validateCreateTimesheet
} from "../helpers/requestValidator.js"

const sequelize = new Sequelize(config.get('pg_name'), config.get('pg_username'), config.get('pg_password'), {
    host: config.get('pg_host'),
    dialect: 'postgres'
});


const Timesheet = sequelize.define("timesheet_status", {
    id: {
        type: Sequelize.INTEGER,
        autoIncrement: true,
        primaryKey: true,
        allowNull: false
    },
    name: {
        type: Sequelize.STRING,
        allowNull: false
    },
    color: {
        type: Sequelize.STRING,
        allowNull: false
    }
});

const TimesheetData = sequelize.define("timesheet", {
    id: {
        type: Sequelize.INTEGER,
        autoIncrement: true,
        primaryKey: true,
        allowNull: false
    },
    timestmap: {
        type: Sequelize.STRING,
        allowNull: false
    },
    user_id: {
        type: Sequelize.STRING,
        allowNull: false
    },
    timesheet_id: {
        type: Sequelize.INTEGER,
        allowNull: false
    }
});

export const createTimesheetStatus = async (req, res) => {
    const body = req.body;
    // validation
    const {error} = validateCreateTimesheetStatus(body);
    if (error) return res.status(400).json({
        success: false,
        message: error
    });

    await Timesheet.sync();

    Timesheet.create({
        name: req.body.name,
    }).then(result => {
        return res.status(200).json({
            success: true,
        });
    }).catch(err => console.log(err));

}

export const updateTimesheetStatus = async (req, res) => {
    const body = req.body;
    // validation
    const {error} = validateUpdateTimesheetStatus(body);
    if (error) return res.status(400).json({
        success: false,
        message: error
    });

    await Timesheet.sync();

    await Timesheet.update({
        name: req.body.name,
    }, {
        where: {
            id: req.body.id,
        }
    }).then(result => {
        return res.status(200).json({
            success: true,
        });
    }).catch(err => console.log(err));

}

export const getTimesheetStatus = async (req, res) => {

    await Timesheet.sync();
    Timesheet.findAll().then(result => {
        return res.status(200).json({
            result,
        });
    }).catch(err => console.log(err));

}


export const createTimesheet = async (req, res) => {
    const body = req.body;
    // validation
    const {error} = validateCreateTimesheet(body);
    if (error) return res.status(400).json({
        success: false,
        message: error
    });

    await TimesheetData.sync();

    let rangeDate = new Map(JSON.parse(req.body.rangeDate))
    if (rangeDate.size > 0) {
        for (const [key, value] of rangeDate.entries()) {
            TimesheetData.create({
                timestmap: value,
                user_id: req.body.user_id,
                timesheet_id: req.body.timesheet_id
            })
        }

        return res.status(200).json({
            success: true,
        });
    } else {
        TimesheetData.create({
            timestmap: req.body.timestmap,
            user_id: req.body.user_id,
            timesheet_id: req.body.timesheet_id
        }).then(result => {
            return res.status(200).json({
                success: true,
            });
        }).catch(err => console.log(err));
    }

}


export const getTimesheet = async (req, res) => {

    /*
        await TimesheetData.sync();
        TimesheetData.findAll().then(result => {
            return res.status(200).json({
                result,
            });
        }).catch(err => console.log(err));
    */


    TimesheetData.belongsTo(Timesheet, {foreignKey: 'timesheet_id'});
    Timesheet.hasMany(TimesheetData, {foreignKey: 'timesheet_id'});
    await TimesheetData.findAll({include: [Timesheet]}).then(result => {
        return res.status(200).json({
            result,
        });
    }).catch(err => console.log(err));

}
