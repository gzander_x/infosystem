import {Projects} from "../models/project.js"
import {Commits} from "../models/commits.js"
import {UserCommits} from "../models/userCommits.js"

export const push = async (req, res) => {

    await Commits.sync();
    await Projects.sync();
    await UserCommits.sync();
    /*   let json = {
            "push": {
                "changes": [{
                    "forced": false,
                    "old": {
                        "name": "master",
                        "links": {
                            "commits": {"href": "https://api.bitbucket.org/2.0/repositories/RomanovV/pfofi.press/commits/master"},
                            "self": {"href": "https://api.bitbucket.org/2.0/repositories/RomanovV/pfofi.press/refs/branches/master"},
                            "html": {"href": "https://bitbucket.org/RomanovV/pfofi.press/branch/master"}
                        },
                        "default_merge_strategy": "merge_commit",
                        "merge_strategies": ["merge_commit", "squash", "fast_forward"],
                        "type": "branch",
                        "target": {
                            "rendered": {},
                            "hash": "e09c9935f63469bd4e75284d59cbbabd55b8fef2",
                            "links": {
                                "self": {"href": "https://api.bitbucket.org/2.0/repositories/RomanovV/pfofi.press/commit/e09c9935f63469bd4e75284d59cbbabd55b8fef2"},
                                "html": {"href": "https://bitbucket.org/RomanovV/pfofi.press/commits/e09c9935f63469bd4e75284d59cbbabd55b8fef2"}
                            },
                            "author": {
                                "raw": "RomanovV <romanovvv1987@gmail.com>",
                                "type": "author",
                                "user": {
                                    "display_name": "Romanov V",
                                    "uuid": "{733b6a92-5fab-45b8-b9bd-7924e4ab0395}",
                                    "links": {
                                        "self": {"href": "https://api.bitbucket.org/2.0/users/%7B733b6a92-5fab-45b8-b9bd-7924e4ab0395%7D"},
                                        "html": {"href": "https://bitbucket.org/%7B733b6a92-5fab-45b8-b9bd-7924e4ab0395%7D/"},
                                        "avatar": {"href": "https://secure.gravatar.com/avatar/83b65d08d2a7b39124db79ba0f327e8b?d=https%3A%2F%2Favatar-management--avatars.us-west-2.prod.public.atl-paas.net%2Finitials%2FRV-4.png"}
                                    },
                                    "type": "user",
                                    "nickname": "RomanovV",
                                    "account_id": "557058:5031f0bc-8a2c-493a-a0d0-6eabfbc06552"
                                }
                            },
                            "summary": {"raw": "test\n", "markup": "markdown", "html": "<p>test</p>", "type": "rendered"},
                            "parents": [{
                                "hash": "a31ff12396ab1f217907160b1726a79a10ebbdd0",
                                "type": "commit",
                                "links": {
                                    "self": {"href": "https://api.bitbucket.org/2.0/repositories/RomanovV/pfofi.press/commit/a31ff12396ab1f217907160b1726a79a10ebbdd0"},
                                    "html": {"href": "https://bitbucket.org/RomanovV/pfofi.press/commits/a31ff12396ab1f217907160b1726a79a10ebbdd0"}
                                }
                            }],
                            "date": "2022-02-16T13:21:16+00:00",
                            "message": "test\n",
                            "type": "commit",
                            "properties": {}
                        }
                    },
                    "links": {
                        "commits": {"href": "https://api.bitbucket.org/2.0/repositories/RomanovV/pfofi.press/commits?include=b925c0b5969f0908c0fc5d4ccd0940a1d55d89b7&exclude=e09c9935f63469bd4e75284d59cbbabd55b8fef2"},
                        "html": {"href": "https://bitbucket.org/RomanovV/pfofi.press/branches/compare/b925c0b5969f0908c0fc5d4ccd0940a1d55d89b7..e09c9935f63469bd4e75284d59cbbabd55b8fef2"},
                        "diff": {"href": "https://api.bitbucket.org/2.0/repositories/RomanovV/pfofi.press/diff/b925c0b5969f0908c0fc5d4ccd0940a1d55d89b7..e09c9935f63469bd4e75284d59cbbabd55b8fef2"}
                    },
                    "created": false,
                    "commits": [{
                        "rendered": {},
                        "hash": "b925c0b5969f0908c0fc5d4ccd0940a1d55d89b7",
                        "links": {
                            "self": {"href": "https://api.bitbucket.org/2.0/repositories/RomanovV/pfofi.press/commit/b925c0b5969f0908c0fc5d4ccd0940a1d55d89b7"},
                            "comments": {"href": "https://api.bitbucket.org/2.0/repositories/RomanovV/pfofi.press/commit/b925c0b5969f0908c0fc5d4ccd0940a1d55d89b7/comments"},
                            "patch": {"href": "https://api.bitbucket.org/2.0/repositories/RomanovV/pfofi.press/patch/b925c0b5969f0908c0fc5d4ccd0940a1d55d89b7"},
                            "html": {"href": "https://bitbucket.org/RomanovV/pfofi.press/commits/b925c0b5969f0908c0fc5d4ccd0940a1d55d89b7"},
                            "diff": {"href": "https://api.bitbucket.org/2.0/repositories/RomanovV/pfofi.press/diff/b925c0b5969f0908c0fc5d4ccd0940a1d55d89b7"},
                            "approve": {"href": "https://api.bitbucket.org/2.0/repositories/RomanovV/pfofi.press/commit/b925c0b5969f0908c0fc5d4ccd0940a1d55d89b7/approve"},
                            "statuses": {"href": "https://api.bitbucket.org/2.0/repositories/RomanovV/pfofi.press/commit/b925c0b5969f0908c0fc5d4ccd0940a1d55d89b7/statuses"}
                        },
                        "author": {
                            "raw": "RomanovV <romanovvv1987@gmail.com>",
                            "type": "author",
                            "user": {
                                "display_name": "Romanov V",
                                "uuid": "{733b6a92-5fab-45b8-b9bd-7924e4ab0395}",
                                "links": {
                                    "self": {"href": "https://api.bitbucket.org/2.0/users/%7B733b6a92-5fab-45b8-b9bd-7924e4ab0395%7D"},
                                    "html": {"href": "https://bitbucket.org/%7B733b6a92-5fab-45b8-b9bd-7924e4ab0395%7D/"},
                                    "avatar": {"href": "https://secure.gravatar.com/avatar/83b65d08d2a7b39124db79ba0f327e8b?d=https%3A%2F%2Favatar-management--avatars.us-west-2.prod.public.atl-paas.net%2Finitials%2FRV-4.png"}
                                },
                                "type": "user",
                                "nickname": "RomanovV",
                                "account_id": "557058:5031f0bc-8a2c-493a-a0d0-6eabfbc06552"
                            }
                        },
                        "summary": {"raw": "test\n", "markup": "markdown", "html": "<p>test</p>", "type": "rendered"},
                        "parents": [{
                            "hash": "e09c9935f63469bd4e75284d59cbbabd55b8fef2",
                            "type": "commit",
                            "links": {
                                "self": {"href": "https://api.bitbucket.org/2.0/repositories/RomanovV/pfofi.press/commit/e09c9935f63469bd4e75284d59cbbabd55b8fef2"},
                                "html": {"href": "https://bitbucket.org/RomanovV/pfofi.press/commits/e09c9935f63469bd4e75284d59cbbabd55b8fef2"}
                            }
                        }],
                        "date": "2022-02-16T13:23:39+00:00",
                        "message": "test\n",
                        "type": "commit",
                        "properties": {}
                    }],
                    "truncated": false,
                    "closed": false,
                    "new": {
                        "name": "master",
                        "links": {
                            "commits": {"href": "https://api.bitbucket.org/2.0/repositories/RomanovV/pfofi.press/commits/master"},
                            "self": {"href": "https://api.bitbucket.org/2.0/repositories/RomanovV/pfofi.press/refs/branches/master"},
                            "html": {"href": "https://bitbucket.org/RomanovV/pfofi.press/branch/master"}
                        },
                        "default_merge_strategy": "merge_commit",
                        "merge_strategies": ["merge_commit", "squash", "fast_forward"],
                        "type": "branch",
                        "target": {
                            "rendered": {},
                            "hash": "b925c0b5969f0908c0fc5d4ccd0940a1d55d89b7",
                            "links": {
                                "self": {"href": "https://api.bitbucket.org/2.0/repositories/RomanovV/pfofi.press/commit/b925c0b5969f0908c0fc5d4ccd0940a1d55d89b7"},
                                "html": {"href": "https://bitbucket.org/RomanovV/pfofi.press/commits/b925c0b5969f0908c0fc5d4ccd0940a1d55d89b7"}
                            },
                            "author": {
                                "raw": "RomanovV <romanovvv1987@gmail.com>",
                                "type": "author",
                                "user": {
                                    "display_name": "Romanov V",
                                    "uuid": "{733b6a92-5fab-45b8-b9bd-7924e4ab0395}",
                                    "links": {
                                        "self": {"href": "https://api.bitbucket.org/2.0/users/%7B733b6a92-5fab-45b8-b9bd-7924e4ab0395%7D"},
                                        "html": {"href": "https://bitbucket.org/%7B733b6a92-5fab-45b8-b9bd-7924e4ab0395%7D/"},
                                        "avatar": {"href": "https://secure.gravatar.com/avatar/83b65d08d2a7b39124db79ba0f327e8b?d=https%3A%2F%2Favatar-management--avatars.us-west-2.prod.public.atl-paas.net%2Finitials%2FRV-4.png"}
                                    },
                                    "type": "user",
                                    "nickname": "RomanovV",
                                    "account_id": "557058:5031f0bc-8a2c-493a-a0d0-6eabfbc06552"
                                }
                            },
                            "summary": {"raw": "test\n", "markup": "markdown", "html": "<p>test</p>", "type": "rendered"},
                            "parents": [{
                                "hash": "e09c9935f63469bd4e75284d59cbbabd55b8fef2",
                                "type": "commit",
                                "links": {
                                    "self": {"href": "https://api.bitbucket.org/2.0/repositories/RomanovV/pfofi.press/commit/e09c9935f63469bd4e75284d59cbbabd55b8fef2"},
                                    "html": {"href": "https://bitbucket.org/RomanovV/pfofi.press/commits/e09c9935f63469bd4e75284d59cbbabd55b8fef2"}
                                }
                            }],
                            "date": "2022-02-16T13:23:39+00:00",
                            "message": "test\n",
                            "type": "commit",
                            "properties": {}
                        }
                    }
                }]
            },
            "actor": {
                "display_name": "Romanov V",
                "uuid": "{733b6a92-5fab-45b8-b9bd-7924e4ab0395}",
                "links": {
                    "self": {"href": "https://api.bitbucket.org/2.0/users/%7B733b6a92-5fab-45b8-b9bd-7924e4ab0395%7D"},
                    "html": {"href": "https://bitbucket.org/%7B733b6a92-5fab-45b8-b9bd-7924e4ab0395%7D/"},
                    "avatar": {"href": "https://secure.gravatar.com/avatar/83b65d08d2a7b39124db79ba0f327e8b?d=https%3A%2F%2Favatar-management--avatars.us-west-2.prod.public.atl-paas.net%2Finitials%2FRV-4.png"}
                },
                "type": "user",
                "nickname": "RomanovV",
                "account_id": "557058:5031f0bc-8a2c-493a-a0d0-6eabfbc06552"
            },
            "repository": {
                "scm": "git",
                "website": null,
                "uuid": "{4a8c4040-fa0a-49c9-829c-010d72d93ec5}",
                "links": {
                    "self": {"href": "https://api.bitbucket.org/2.0/repositories/RomanovV/pfofi.press"},
                    "html": {"href": "https://bitbucket.org/RomanovV/pfofi.press"},
                    "avatar": {"href": "https://bytebucket.org/ravatar/%7B4a8c4040-fa0a-49c9-829c-010d72d93ec5%7D?ts=php"}
                },
                "project": {
                    "links": {
                        "self": {"href": "https://api.bitbucket.org/2.0/workspaces/RomanovV/projects/PROJ"},
                        "html": {"href": "https://bitbucket.org/RomanovV/workspace/projects/PROJ"},
                        "avatar": {"href": "https://bitbucket.org/account/user/RomanovV/projects/PROJ/avatar/32?ts=1543611527"}
                    },
                    "type": "project",
                    "name": "Untitled project",
                    "key": "PROJ",
                    "uuid": "{8423b132-6a1c-4256-bf82-3cba3ae96674}"
                },
                "full_name": "RomanovV/pfofi.press",
                "owner": {
                    "display_name": "Romanov V",
                    "uuid": "{733b6a92-5fab-45b8-b9bd-7924e4ab0395}",
                    "links": {
                        "self": {"href": "https://api.bitbucket.org/2.0/users/%7B733b6a92-5fab-45b8-b9bd-7924e4ab0395%7D"},
                        "html": {"href": "https://bitbucket.org/%7B733b6a92-5fab-45b8-b9bd-7924e4ab0395%7D/"},
                        "avatar": {"href": "https://secure.gravatar.com/avatar/83b65d08d2a7b39124db79ba0f327e8b?d=https%3A%2F%2Favatar-management--avatars.us-west-2.prod.public.atl-paas.net%2Finitials%2FRV-4.png"}
                    },
                    "type": "user",
                    "nickname": "RomanovV",
                    "account_id": "557058:5031f0bc-8a2c-493a-a0d0-6eabfbc06552"
                },
                "workspace": {
                    "slug": "RomanovV",
                    "type": "workspace",
                    "name": "Romanov V",
                    "links": {
                        "self": {"href": "https://api.bitbucket.org/2.0/workspaces/RomanovV"},
                        "html": {"href": "https://bitbucket.org/RomanovV/"},
                        "avatar": {"href": "https://bitbucket.org/workspaces/RomanovV/avatar/?ts=1543611527"}
                    },
                    "uuid": "{733b6a92-5fab-45b8-b9bd-7924e4ab0395}"
                },
                "type": "repository",
                "is_private": true,
                "name": "pfofi.press"
            }
        }*/
    /*    const __dirname = path.resolve()
        let filePath = __dirname + '/public/data.txt';
        var writer = fs.createWriteStream( filePath);
        let response = {
            name: '',
            id: ''
        }
       // writer.write(JSON.stringify(response));

        writer.write(JSON.stringify(req.body));*/

    let body = req.body
    let nameUser = ''
    // let body  = json

    for (let d of body.push.changes) {
        Projects.findOne({
            attributes: ['id'],
            where: {
                repo_name: body.repository.name
            }
        }).then(result => {
            nameUser  = d.commits[0].author.user.display_name
            Commits.create({
                href: d.links.html.href,
                hash: d.commits[0].hash,
                author: d.commits[0].author.user.display_name,
                date: d.commits[0].date,
                text: d.commits[0].message,
                type: d.commits[0].type,
                project_id: result.id
            }).then(res => {
                console.log(res);

                UserCommits.create({
                    name: nameUser
                }).then(res => {
                    console.log(res);
                }).catch(err => console.log(err));


            }).catch(err => console.log(err));

        }).catch(err => console.log(err));


    }
    res.send('ok')


}


