import axios from 'axios'
import {Sequelize} from 'sequelize'
import config from "config";

const sequelize = new Sequelize(config.get('pg_name'), config.get('pg_username'), config.get('pg_password'), {
    host: config.get('pg_host'),
    dialect: 'postgres'
});

export const getTasks = async (req, res) => {
    const Tasks = sequelize.define("tasks", {
        id: {
            type: Sequelize.INTEGER,
            autoIncrement: true,
            primaryKey: true,
            allowNull: false
        },
        task_id: {
            type: Sequelize.STRING,
            allowNull: true
        },
        project_id: {
            type: Sequelize.STRING,
            allowNull: true
        },
        name: {
            type: Sequelize.STRING,
            allowNull: true
        },
        id_readable: {
            type: Sequelize.STRING,
            allowNull: true
        },
        description: {
            type: Sequelize.STRING,
            allowNull: true
        },
        estimation: {
            type: Sequelize.INTEGER,
            allowNull: true
        },
        subsystem: {
            type: Sequelize.STRING,
            allowNull: true
        },
        state: {
            type: Sequelize.STRING,
            allowNull: true
        },
        type: {
            type: Sequelize.STRING,
            allowNull: true
        },
        created: {
            type: Sequelize.INTEGER,
            allowNull: true
        },
        trial407: {
            type: Sequelize.STRING,
            allowNull: true
        },
    });


    const Task_spent_time = sequelize.define("task_spent_time", {
        id: {
            type: Sequelize.INTEGER,
            autoIncrement: true,
            primaryKey: true,
            allowNull: false
        },
        date: {
            type: Sequelize.BIGINT(),
            allowNull: false
        },
        task_id: {
            type: Sequelize.STRING,
            allowNull: false
        },
        minutes: {
            type: Sequelize.INTEGER,
            allowNull: false
        },
        name: {
            type: Sequelize.STRING,
            allowNull: false
        },
        trial407: {
            type: Sequelize.STRING,
            allowNull: true
        },
    });

    await Task_spent_time.sync({force: true});
    await Tasks.sync({force: true});

    let configConnect = {
        method: 'get',
        url: config.get('api_host') + '/api/issues?skip=0&top=1000&fields=idReadable,id,summary,description,project(id),customFields(name,value(name,localizedName,minutes))&customFields=state&customFields=type&customFields=estimation',
        headers: {
            'Authorization': config.get('token')
        }
    };

    axios(configConnect)
        .then(function (response) {
            for (let d of response.data) {

            Tasks.create({
                    task_id: d.id,
                    project_id:d.project.id,
                    name: d.summary,
                    id_readable: d.idReadable,
                    description:d.description,
                }).then(res => {
                    console.log(res);
                }).catch(err => console.log(err));

                prom.push(
                    axios({
                        method: 'get',
                        //{{host}}/api/issues/2-11547?fields=created,updater(name),customFields(name,value(minutes,presentation))&customFields=Estimation&customFields=Spent time
                        url: `http://94.127.67.113:8070/api/issues/${d.id}/timeTracking?fields=workItems(created,duration(minutes),creator(name))`,
                        // responseType: 'arraybuffer',
                        headers: {
                            'Content-Type': 'application/json',
                            Authorization: config.get('token')
                        }
                    })
                )
            }

            Promise.all(prom).then(r => {

                for (let o of r) {
                    let task_id = o.request.path.split('/')[3]
                    if (o.data.workItems.length > 0) {
                        for (let w of o.data.workItems) {
                            Task_spent_time.create({
                                task_id: task_id,
                                date: w.created,
                                name: w.creator.name,
                                minutes: w.duration.minutes,
                            }).then(res => {
                                console.log(res);
                            }).catch(err => console.log(err));

                        }
                    }
                }

                res.json('Success');

            })
        })
        .catch(function (error) {
            res.json(error);
        });

}