import express from 'express'
import createError from 'http-errors'
import path from 'path'
import http from 'http'
import accesslog from 'access-log'

import usersRouter from './routes/users.js'
import tasksRouter from './routes/tasks.js'
import projectProject from './routes/project.js'
import indexRouter from './routes/index.js'
import skillsRouter from './routes/skills.js'
import timesheetRouter from './routes/timesheet.js'
import commitsRouter from './routes/commits.js'
import config from 'config'
import cors from 'cors'
import bodyParser from 'body-parser'


var app = express();
const __dirname = path.resolve()
const port  = config.get('port') || 3000
app.use(cors())



app.use(bodyParser.urlencoded({ extended: false }))
app.use(bodyParser.json())

app.use(usersRouter);
app.use(tasksRouter);
app.use(projectProject);
app.use(indexRouter);
app.use(skillsRouter);
app.use(timesheetRouter);
app.use(commitsRouter);


// catch 404 and forward to error handler
app.use(function(req, res, next) {
  next(createError(404));
});

// error handler
app.use(function(err, req, res, next) {
  // set locals, only providing error in development
  res.locals.message = err.message;
  res.locals.error = req.app.get('env') === 'development' ? err : {};

  // render the error page
  res.status(err.status || 500);
  res.send(err)
  //res.render('error');
});


app.listen(port);


