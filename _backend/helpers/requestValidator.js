import Joi from "joi";


export const validateCreateUser = (input) => {
    const schema = Joi.object({
        fio: Joi.string()
            .required(),

        position: Joi.string()
            .required(),

        date_start: Joi.string()
            .required(),

        form_cooperation: Joi.string(),
        requisites: Joi.array(),
        skills: Joi.array(),
        projects: Joi.array(),
    })

    return schema.validate(input);
}
export const validateUpdateUser = (input) => {
    const schema = Joi.object({
        id: Joi.string()
            .required(),
        fio: Joi.string()
            .required(),

        position: Joi.string()
            .required(),

        date_start: Joi.string()
            .required(),

        form_cooperation: Joi.string(),
        requisites: Joi.array(),
        skills: Joi.array(),
        projects: Joi.array(),
    })

    return schema.validate(input);
}


export const validateCreateSkills = (input) => {
    const schema = Joi.object({
        name: Joi.string()
            .required(),
        knowledge_id: Joi.string()
            .required(),
    })

    return schema.validate(input);
}
export const validateUpdateSkills = (input) => {
    const schema = Joi.object({
        id: Joi.string()
            .required(),
        name: Joi.string()
            .required(),
        knowledge_id: Joi.string()
            .required(),
    })

    return schema.validate(input);
}

export const validateCreateTimesheetStatus = (input) => {
    const schema = Joi.object({
        name: Joi.string()
            .required()
    })

    return schema.validate(input);
}
export const validateUpdateTimesheetStatus = (input) => {
    const schema = Joi.object({
        name: Joi.string()
            .required()
    })

    return schema.validate(input);
}

export const validateCreateTimesheet = (input) => {
    const schema = Joi.object({

        timestmap: Joi.string()
            .required(),
        user_id: Joi.number()
            .required(),
        timesheet_id: Joi.number()
            .required(),
        rangeDate: Joi.string()
    })

    return schema.validate(input);
}