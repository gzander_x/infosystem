import {Sequelize} from "sequelize";
import config from "config";

const sequelize = new Sequelize(config.get('pg_name'), config.get('pg_username'), config.get('pg_password'), {
    host: config.get('pg_host'),
    dialect: 'postgres'
});

export const  Projects = sequelize.define("projects", {
    id: {
        type: Sequelize.STRING,
        primaryKey: true,
        allowNull: false
    },
    name: {
        type: Sequelize.STRING,
        allowNull: false
    },
    short_name: {
        type: Sequelize.STRING,
        allowNull: false
    },
    trial407: {
        type: Sequelize.STRING,
        allowNull: false
    },
    skills: {
        type: Sequelize.JSON,
        allowNull: false
    },
    payments: {
        type: Sequelize.JSON,
        allowNull: false
    }
});