import {Sequelize} from "sequelize";
import config from "config";

const sequelize = new Sequelize(config.get('pg_name'), config.get('pg_username'), config.get('pg_password'), {
    host: config.get('pg_host'),
    dialect: 'postgres'
});

export const  UserCommits = sequelize.define("user_commits", {
    id: {
        type: Sequelize.INTEGER,
        autoIncrement: true,
        primaryKey: true,
        allowNull: false
    },
    name: {
        type: Sequelize.STRING,
        allowNull: false,
        unique: true
    },
    user_id: {
        type: Sequelize.INTEGER,
        allowNull: true
    },

});