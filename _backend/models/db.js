import {Sequelize} from "sequelize";
import config from "config";

export const db = new Sequelize(config.get('pg_name'), config.get('pg_username'), config.get('pg_password'), {
    host: config.get('pg_host'),
    dialect: 'postgres'
});
