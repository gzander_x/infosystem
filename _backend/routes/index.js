import {Router} from 'express'
import {
    migUsers,
    migTasks,
    planFact,
    users,
    tasks,
    planFactByPlatform,
    commitsByProject, commitsByUser
} from "../controllers/index.js";
import {createUser,updateUser,getUser} from "../controllers/users.js";
const router = new Router()

router.post('/api/mig_users', migUsers);
router.post('/api/mig_tasks', migTasks);
router.post('/api/plan_fact', planFact);
router.post('/api/users', users);
router.post('/api/tasks', tasks);
router.post('/api/plan_fact_by_platform', planFactByPlatform);
router.post('/api/tasks_by_platform', planFactByPlatform);
router.post('/api/commitsByProject', commitsByProject);
router.post('/api/commitsByUser', commitsByUser);

//user
router.post('/api/user/create', createUser);
router.post('/api/user/update', updateUser);
router.get('/api/user', getUser);

export default router
