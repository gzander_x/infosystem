import {Router} from 'express'
import {push} from "../controllers/commits.js";

const router = new Router()

router.post('/commits/push', push);

export default router
