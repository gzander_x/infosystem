import {Router} from 'express'
import {getTasks} from "../controllers/tasks.js";

const router = new Router()

router.get('/tasks', getTasks);

export default router
