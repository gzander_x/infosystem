import {Router} from 'express'
import {getUser,importUsers} from "../controllers/users.js";

const router = new Router()

router.get('/users', getUser);
router.get('/users/import', importUsers);

export default router
