import {Router} from 'express'
import {createSkill, updateSkill, getSkills} from "../controllers/skills.js";

const router = new Router()

router.post('/api/skills/create', createSkill);
router.post('/api/skills/update', updateSkill);
router.get('/api/skills', getSkills);


export default router
