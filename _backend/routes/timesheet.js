import {Router} from 'express'
import {createTimesheetStatus, updateTimesheetStatus, getTimesheetStatus, createTimesheet,getTimesheet} from "../controllers/timesheet.js";

const router = new Router()

router.post('/api/timesheet/status-create', createTimesheetStatus);
router.post('/api/timesheet/status-update', updateTimesheetStatus);
router.get('/api/timesheet/status', getTimesheetStatus);
router.post('/api/timesheet/create', createTimesheet);
router.get('/api/timesheet',getTimesheet);

export default router
