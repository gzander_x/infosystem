import {Router} from 'express'
import {getProject} from "../controllers/project.js";

const router = new Router()

router.get('/api/project', getProject);

export default router
