module.exports = {
  /*
   ** Headers of the page
   */
  server: {
    port: 8092
  },
  head: {
    title: 'Winfox Dashboard',
    meta: [
      { charset: 'utf-8' },
      { name: 'viewport', content: 'width=device-width, initial-scale=1' },
      { hid: 'description', name: 'description', content: 'Nuxt.js project' }
    ],
    link: [
      { rel: 'icon', type: 'image/x-icon', href: '/favicon.ico' },
      {
        rel: 'stylesheet',
        href:
          'https://fonts.googleapis.com/css2?family=Roboto:wght@100;300;500&display=swap'
      }
    ]
  },
  plugins: [
    { src: '~plugins/axios', mode: 'client' },
    { src: '~plugins/vue2-datepicker.js', mode: 'client' },
    { src: '~plugins/vue-excel-editor' ,mode: 'client'}
  ],
  /*
   ** Customize the progress bar color
   */
  loading: { color: '#3B8070' },
  /*
   ** Build configuration
   */
  serverMiddleware: [
    // { path: '/api', handler: require('body-parser').json() },
    // {
    //   path: '/api',
    //   handler: (req, res, next) => {
    //     const url = require('url')
    //     req.query = url.parse(req.url, true).query
    //     req.params = { ...req.query, ...req.body }
    //     next()
    //   }
    // },
    // { path: '/api', handler: '~/serverMiddleware/api-server.js' }
   // '~/api/index.js'
  ],
  modules: ['cookie-universal-nuxt', '@nuxtjs/axios','bootstrap-vue/nuxt'],
  bootstrapVue: {
    bootstrapCSS: false, // Or `css: false`
    bootstrapVueCSS: false, // Or `bvCSS: false`
    icons: true
  },
  buildModules: ['@nuxtjs/moment'],
  axios: {
    https: true,
    credentials: true,
    headers: {
      common: {
        Accept: 'application/json, text/plain, */*'
      }
    }
  },
  //css: ['normalize.css/normalize.css'],
  css: ['@/assets/scss/custom.scss'],
  build: {
    /*
     ** Run ESLint on save
     */
    extend (config, { isDev, isClient }) {
      if (isDev && isClient) {
        config.module.rules.push({
          enforce: 'pre',
          test: /\.(js|vue)$/,
          loader: 'eslint-loader',
          exclude: /(node_modules)/
        })
      }
    }
  }
}
