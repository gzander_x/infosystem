import Vue from 'vue'
import vueSlideUpDown from 'vue-slide-up-down'

if (process.client) {
  Vue.component('vue-slide-up-down', vueSlideUpDown)
}
