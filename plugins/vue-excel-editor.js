import Vue from 'vue'
import VueExcelEditor from 'vue-excel-editor'

Vue.use(VueExcelEditor)

if (process.client) {
  Vue.component('VueExcelEditor', VueExcelEditor)
}

