import Vue from 'vue'
import DatePicker from 'vue2-datepicker';
import 'vue2-datepicker/index.css';
import 'vue2-datepicker/locale/en';

if (process.client) {
  Vue.component('DatePicker', DatePicker)
}
