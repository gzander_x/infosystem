const axios = require('axios')
const db = require('./dbClient')
const key =
  'perm:YWRtaW4=.NDctMQ==.pfgnDyqz9rkwCTECZ0inI6XvO5RWPt'

axios({
  method: 'get',
  url:
    'http://94.127.67.113:8070/api/issues?skip=0&top=1000&fields=idReadable,id,summary,description,project(id),customFields(name,value(name,localizedName,minutes))&customFields=state&customFields=type&customFields=estimation',
  // responseType: 'arraybuffer',
  headers: {
    'Content-Type': 'application/json',
    Authorization: 'Bearer ' + key
  }
}).then(function (response) {
  let addValues = [],
    addInsertColumn = [],
    prom = []

  for (let d of response.data) {
    addValues.push(d.id)
    addValues.push(d.project.id)
    addValues.push(d.summary)
    addValues.push(d.idReadable)
    addValues.push(d.description)
    addInsertColumn.push('(?,?,?,?,?)')
    prom.push(
      axios({
        method: 'get',
        //{{host}}/api/issues/2-11547?fields=created,updater(name),customFields(name,value(minutes,presentation))&customFields=Estimation&customFields=Spent time
        url: `http://94.127.67.113:8070/api/issues/${d.id}/timeTracking?fields=workItems(created,duration(minutes),creator(name))`,
        // responseType: 'arraybuffer',
        headers: {
          'Content-Type': 'application/json',
          Authorization: 'Bearer ' + key
        }
      })
    )
  }

  // prom.push(
  new Promise(function (resolve, reject) {
    q = `insert OR IGNORE INTO tasks (task_id, project_id, name, id_readable, description) values ${addInsertColumn.join(
      ','
    )}`
    db.insert(q, addValues, function (err, data) {
      if (err) {
        reject(data)
      } else {
        resolve(data)
      }
    })
  })
  // )

  Promise.all(prom).then(r => {
    let insert = [],
      values = []

    for (let o of r) {
      let task_id = o.request.path.split('/')[3]
      if (o.data.workItems.length > 0) {
        for (let w of o.data.workItems) {
          values.push(task_id)
          values.push(w.created)
          values.push(w.creator.name)
          values.push(w.duration.minutes)
          insert.push('(?,?,?,?)')
        }
      }

      // console.log(22222)
      // console.log(Object.keys(o[0]))
    }
    q = `insert OR IGNORE INTO task_spent_time (task_id, date, name, minutes) values ${insert.join(
      ','
    )}`
    db.insert(q, values, function (err, data) {
      if (err) {
        console.log(1111)
        console.log(data)
      } else {
        console.log(2222)
        console.log(data)
      }
    })
  })
})
