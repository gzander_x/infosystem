const dbTemp = require('sqlite3').verbose()

const tepmDB = new dbTemp.Database(__dirname+'/youtrack_db.db', err => {
  if (err) {
    console.error(err.message)
  }
  console.log('Connected to the chinook database.')
})

module.exports.insert = function (query, params, callback) {
  tepmDB.run(query, params, function (err) {
    if (err) {
      return callback(true, err.message)
    } else {
      callback(false, this.lastID)
    }
  })
  //   tepmDB.each(query, (err, row) => {
  //     if (err) {
  //       callback(true, err.message)
  //     } else {
  //       callback(false, this.lastID)
  //     }
  //   })
}
module.exports.select = function (query, params, callback) {
  tepmDB.get(query, params, function (err, rows) {
    if (err) {
      return callback(true, err.message)
    } else {
      callback(false, rows)
    }
  })
  //   tepmDB.each(query, (err, row) => {
  //     if (err) {
  //       callback(true, err.message)
  //     } else {
  //       callback(false, this.lastID)
  //     }
  //   })
}
module.exports.table = function (query, params, callback) {
  tepmDB.all(query, params, function (err, rows) {
    if (err) {
      return callback(true, err.message)
    } else {
      callback(false, rows)
    }
  })
}
