const express = require('express')
const app = express()
//const db = require('./dbClient')
const axios = require('axios')
const bodyParser = require('body-parser')
const cors = require('cors')
const moment = require('moment')
const key = 'perm:YWRtaW4=.NDctMQ==.pfgnDyqz9rkwCTECZ0inI6XvO5RWPt'
// var CronJob = require('cron').CronJob
// var job = new CronJob(
//   '* */1 * * * *',
//   function () {
//     console.log('You will see this message every second    ')
//   },
//   null,
//   true,
//   'Europe/Moscow'
// )
// job.start()
app.use(bodyParser.urlencoded({ extended: false }))
app.use(bodyParser.json())
app.use((required, response, next) => {
  if (required.method == 'OPTIONS') {
    response.status(200).send({})
  } else {
    next()
  }
})
app.use(cors())
app.options('*', cors())

app.get('/mig_users', (req, res) => {
  axios({
    method: 'get',
    // url: 'http://94.127.67.113:8070/api/issues/2-1433/timeTracking?fields=workItems(created,duration(minutes),creator(name))',
    url:
      'http://94.127.67.113:8070/hub/api/rest/projectteams/?$top=-1&fields=id,project(name),name,users(name)',
    // responseType: 'arraybuffer',
    headers: {
      'Content-Type': 'application/json',
      Authorization: 'Bearer ' + key
    }
  }).then(function (response) {
    let insert = [],
      values = []
    for (let p of response.data.projectteams) {
      if (p.users) {
        for (let u of p.users) {
          values.push(p.project.name)
          values.push(u.name)

          insert.push('(?,?)')
        }
      }
    }
    q = `insert OR IGNORE INTO project_users (project, name) values ${insert.join(
      ','
    )}`
    db.insert(q, values, function (err, data) {
      if (err) {
        res.status(500).send(data)
      } else {
        res.send('End migration')
      }
    })
  })
})
app.get('/mig_tasks', (req, res) => {
  axios({
    method: 'get',
    url:
      // 'http://94.127.67.113:8070/api/issues?skip=0&top=1000&fields=idReadable,id,summary,description,project(id),customFields(name,value(name,localizedName,minutes))&customFields=state&customFields=type&customFields=estimation',
      'http://94.127.67.113:8070/api/issues?skip=0&top=1000&fields=idReadable,id,created,summary,description,project(id),customFields(name,value(name,localizedName,minutes))&customFields=estimation&customFields=Subsystem&customFields=state&&customFields=Type',
    // responseType: 'arraybuffer',
    headers: {
      'Content-Type': 'application/json',
      Authorization: 'Bearer ' + key
    }
  }).then(function (response) {
    let addValues = [],
      addInsertColumn = [],
      prom = []

    for (let d of response.data) {
      let st = new Date(d.created),
        unix = moment(st).unix(),
        addKeys = {}
      addKeys.id = d.id
      addKeys.project = d.project.id
      addKeys.summary = d.summary
      addKeys.idReadable = d.idReadable
      addKeys.description = d.description
      let subsystem = null,
        estimation = 0,
        state = null,
        type = null

      for (let c of d.customFields) {
        if (c.name == 'Subsystem' && c.value) {
          subsystem = c.value.name
        }
        if (c.name == 'Estimation' && c.value) {
          estimation = c.value.minutes
        }
        if (c.name == 'Type' && c.value) {
          type = c.value.localizedName
            ? c.value.localizedName.toLowerCase()
            : c.value.name.toLowerCase()
        }
        if (c.name == 'State') {
          if (c.value.localizedName != null) {
            state = c.value.localizedName.toLowerCase()
          } else {
            state = c.value.name.toLowerCase()
          }
        }
      }
      addKeys.estimation = estimation
      addKeys.subsystem = subsystem
      addKeys.state = state
      addKeys.type = type
      addKeys.unix = unix
      // addValues = Object.assign([], addValues, Object.values(addKeys))
      addValues = addValues.concat(Object.values(addKeys))


      addInsertColumn.push('(?,?,?,?,?,?,?,?,?,?)')
      prom.push(
        axios({
          method: 'get',
          url: `http://94.127.67.113:8070/api/issues/${d.id}/timeTracking?fields=workItems(created,duration(minutes),creator(name))`,
          // responseType: 'arraybuffer',
          headers: {
            'Content-Type': 'application/json',
            Authorization: 'Bearer ' + key
          }
        })
      )
    }

    // prom.push(
    new Promise(function (resolve, reject) {
      q = `insert INTO tasks (task_id, project_id, name, id_readable, description, estimation,subsystem,state, type, created) values ${addInsertColumn.join(
        ','
      )} ON CONFLICT(task_id) DO UPDATE SET subsystem = excluded.subsystem,estimation=excluded.estimation,state=excluded.state,type=excluded.type`
      db.insert(q, addValues, function (err, data) {
        if (err) {
          // res.status(500).send(data)
        } else {
          // res.send('End migration')
        }
      })
    })
    // )

    Promise.all(prom).then(r => {
      let insert = [],
        values = []

      for (let o of r) {
        let task_id = o.request.path.split('/')[3]
        if (o.data.workItems.length > 0) {
          for (let w of o.data.workItems) {
            let st = new Date(w.created),
              unix = moment(st).unix()
            values.push(task_id)
            values.push(unix)
            values.push(w.creator.name)
            values.push(w.duration.minutes)
            insert.push('(?,?,?,?)')
          }
        }

        // console.log(22222)
        // console.log(Object.keys(o[0]))
      }
      q = `insert OR IGNORE INTO task_spent_time (task_id, date, name, minutes) values ${insert.join(
        ','
      )}`
      db.insert(q, values, function (err, data) {
        if (err) {
          res.status(500).send(data)
        } else {
          res.send('End migration')
        }
      })
    })
  })
})
app.get('/mig_projects', (req, res) => {
  axios({
    method: 'get',
    url:
      'http://94.127.67.113:8070/api/admin/projects?fields=id,name,shortName&$skip=0&$top=1000',
    // responseType: 'arraybuffer',
    headers: {
      'Content-Type': 'application/json',
      Authorization: 'Bearer ' + key
    }
  }).then(function (response) {
    let addValues = [],
      addInsertColumn = []

    for (let d of response.data) {
      addValues.push(d.id)
      addValues.push(d.name)
      addValues.push(d.shortName)
      addInsertColumn.push('(?,?,?)')
    }
    q = `insert INTO projects (id, name, short_name) values ${addInsertColumn.join(
      ','
    )} ON CONFLICT(id) DO UPDATE SET name = name`
    db.insert(q, addValues, function (err, data) {
      if (err) {
        res.status(500).send(data)
      } else {
        res.send('End migration')
      }
    })
  })
})
app.post('/plan_fact', (req, res) => {
  let filterValue = []
  if (req.body.period) {
    if (
      parseInt(req.body.period.value[0]) &&
      parseInt(req.body.period.value[1])
    ) {
      filterValue.push('t.created>=' + req.body.period.value[0])
      filterValue.push('t.created<=' + req.body.period.value[1])
    }
  }

  // res.status(200).json({ message: "I work" });
  Promise.all([
    new Promise(function (resolve, reject) {
      q = `select * from projects`
      db.table(q, [], function (err, data) {
        if (err) {
          console.log(111)
          console.log(data)
          reject(data)
        } else {
          resolve(data)
        }
      })
    }),
    new Promise(function (resolve, reject) {
      q = `select sum(tst.minutes) as minutes,sum(t.estimation) as estimation, t.project_id from
      task_spent_time as tst
      left join tasks as t on t.task_id = tst.task_id
      ${filterValue.length > 0 ? ' where ' + filterValue.join(' and ') : ''}
      group by t.project_id
      `
      db.table(q, [], function (err, data) {
        if (err) {
          console.log(111)
          console.log(data)
          reject(data)
        } else {
          let st = {}
          for (let d of data) {
            st[d.project_id] = {
              spent_time: d.minutes,
              estimation: d.estimation
            }
          }
          resolve(st)
        }
      })
    })
  ])
    .then(([projectL, spentTime]) => {
      let project = {}

      for (let p of projectL) {
        if (spentTime[p.id]) {
          p.spent_time = spentTime[p.id].spent_time
          p.estimation = spentTime[p.id].estimation
          project[p.id] = p
        }
      }

      res.send(project)
    })
    .catch(err => {
      res.status(500).send(err)
    })
})
app.post('/users', (req, res) => {
  let filterValue = []
  if (req.body.period) {
    if (
      parseInt(req.body.period.value[0]) &&
      parseInt(req.body.period.value[1])
    ) {
      filterValue.push('t.created>=' + req.body.period.value[0])
      filterValue.push('t.created<=' + req.body.period.value[1])
    }
  }
  q = `select tst.name, t.state, t.estimation, tst.minutes, t.state, t.type from task_spent_time tst
  left join tasks t on tst.task_id = t.task_id
  ${filterValue.length > 0 ? ' where ' + filterValue.join(' and ') : ''}`
  db.table(q, [], function (err, data) {
    if (err) {
      console.log(111)
          console.log(data)
      reject(data)
    } else {
      let users = {},
        closeState = ['решена', 'закрыта']
      for (let d of data) {
        if (!users[d.name]) {
          users[d.name] = {
            estimation: 0,
            spent_time: 0,
            complete_task_count: 0,
            open_task_count: 0,
            count_task: 0,
            count_task_error: 0
          }
        }

        if (closeState.includes(d.state)) {
          users[d.name].spent_time += d.minutes
        }
        if (closeState.includes(d.state)) {
          users[d.name].spent_time += d.minutes
          ++users[d.name].complete_task_count
        } else {
          users[d.name].estimation += d.estimation
          ++users[d.name].open_task_count
        }
        if (d.type == 'задание') {
          ++users[d.name].count_task
        }
        if (d.type == 'ошибка') {
          ++users[d.name].count_task_error
        }
      }
      res.send(users)
    }
  })
})
app.post('/tasks', (req, res) => {
  // res.status(200).json({ message: "I work" });
  let filterValue = []
  if (req.body.period) {
    if (
      parseInt(req.body.period.value[0]) &&
      parseInt(req.body.period.value[1])
    ) {
      filterValue.push('t.created>=' + req.body.period.value[0])
      filterValue.push('t.created<=' + req.body.period.value[1])
    }
  }

  Promise.all([
    new Promise(function (resolve, reject) {
      q = `select * from projects`
      db.table(q, [], function (err, data) {
        if (err) {
          console.log(111)
          console.log(data)
          reject(data)
        } else {
          resolve(data)
        }
      })
    }),
    new Promise(function (resolve, reject) {
      q = `select t.project_id, count(tAll.task_id) as tatal_tasks,count(tCom.task_id) as tasks_complete from
      tasks as t
      left join tasks as tAll on tAll.task_id = t.task_id
      left join tasks as tCom on tCom.task_id = t.task_id and tCom.state in ('решена','закрыта')
      ${filterValue.length > 0 ? ' where ' + filterValue.join(' and ') : ''}
      group by t.project_id
      `
      db.table(q, [], function (err, data) {
        if (err) {
          console.log(111)
          console.log(data)
          reject(data)
        } else {
          let st = {}
          for (let d of data) {
            st[d.project_id] = {
              total: d.tatal_tasks,
              complete: d.tasks_complete
            }
          }
          resolve(st)
        }
      })
    })
  ])
    .then(([projectL, countTasks]) => {
      let project = {}
      for (let p of projectL) {
        if (countTasks[p.id]) {
          p.total = countTasks[p.id].total
          p.complete = countTasks[p.id].complete
          project[p.id] = p
        } else {
          p.total = 0
          p.complete = 0
          project[p.id] = p
        }
      }

      res.send(project)
    })
    .catch(err => {
      res.status(500).send(err)
    })
})
app.get('/test', (req, res) => {
  axios({
    method: 'get',
    url:
      // 'http://94.127.67.113:8070/api/issues?skip=0&top=1000&fields=idReadable,id,summary,description,project(id),customFields(name,value(name,localizedName,minutes))&customFields=state&customFields=type&customFields=estimation',
      // 'http://94.127.67.113:8070/api/issues?skip=0&top=1000&fields=idReadable,id,summary,description,project(id),customFields(name,value(name,localizedName,minutes))&customFields=estimation',
      // 'http://94.127.67.113:8070/api/issues/2-12098?fields=customFields(name,value(name,localizedName))',
      // 'http://94.127.67.113:8070/api/issues?skip=0&top=1000&fields=idReadable,id,summary,description,project(id),customFields(name,value(name,localizedName,minutes))&customFields=estimation&customFields=estimation&customFields=Subsystem&customFields=Type',
      // 'http://94.127.67.113:8070/api/issues/2-11547/timeTracking?fields=workItems(created,duration(minutes),creator(name))',
      'http://94.127.67.113:8070/api/issues?skip=0&top=1000&fields=idReadable,id,created,summary,description,project(id),customFields(name,value(name,localizedName,minutes))&customFields=estimation&customFields=Subsystem&customFields=state&&customFields=Type',
    // responseType: 'arraybuffer',
    headers: {
      'Content-Type': 'application/json',
      Authorization: 'Bearer ' + key
    }
  })
    .then(function (response) {
      res.send(response.data)
    })
    .catch(err => {})
})
app.post('/plan_fact_by_platform', (req, res) => {
  let filterValue = []
  if (req.body.period) {
    if (
      parseInt(req.body.period.value[0]) &&
      parseInt(req.body.period.value[1])
    ) {
      filterValue.push('t.created>=' + req.body.period.value[0])
      filterValue.push('t.created<=' + req.body.period.value[1])
    }
  }
  // res.status(200).json({ message: "I work" });
  Promise.all([
    new Promise(function (resolve, reject) {
      q = `select * from projects`
      db.table(q, [], function (err, data) {
        if (err) {
          console.log(111)
          console.log(data)
          reject(data)
        } else {
          let st = {}
          for (let d of data) {
            st[d.id] = {
              name: d.name,
              short_name: d.short_name
            }
          }
          resolve(st)
        }
      })
    }),
    new Promise(function (resolve, reject) {
      q = `select sum(tst.minutes) as minutes,sum(t.estimation) as estimation,t.subsystem, t.project_id from
      task_spent_time as tst
      left join tasks as t on t.task_id = tst.task_id
      where lower(t.subsystem) = 'android' ${
        filterValue.length > 0 ? ' and ' + filterValue.join(' and ') : ''
      }
      group by t.project_id
      `
      db.table(q, [], function (err, data) {
        if (err) {
          console.log(111)
          console.log(data)
          reject(data)
        } else {
          let st = {}
          for (let d of data) {
            st[d.project_id] = {
              spent_time: d.minutes,
              subsystem: d.subsystem,
              estimation: d.estimation
            }
          }
          resolve(st)
        }
      })
    }),
    new Promise(function (resolve, reject) {
      q = `select sum(tst.minutes) as minutes,sum(t.estimation) as estimation,t.subsystem, t.project_id from
      task_spent_time as tst
      left join tasks as t on t.task_id = tst.task_id
      where lower(t.subsystem) = 'ios' ${
        filterValue.length > 0 ? ' and ' + filterValue.join(' and ') : ''
      }
      group by t.project_id
      `
      db.table(q, [], function (err, data) {
        if (err) {
          console.log(111)
          console.log(data)
          reject(data)
        } else {
          let st = {}
          for (let d of data) {
            st[d.project_id] = {
              spent_time: d.minutes,
              subsystem: d.subsystem,
              estimation: d.estimation
            }
          }
          resolve(st)
        }
      })
    }),
    new Promise(function (resolve, reject) {
      q = `select sum(tst.minutes) as minutes,sum(t.estimation) as estimation,t.subsystem, t.project_id from
      task_spent_time as tst
      left join tasks as t on t.task_id = tst.task_id
      where (lower(t.subsystem) != 'android' and lower(t.subsystem) != 'ios')  or t.subsystem ISNULL  ${
        filterValue.length > 0 ? ' and ' + filterValue.join(' and ') : ''
      }
      group by t.project_id
      `
      db.table(q, [], function (err, data) {
        if (err) {
          console.log(111)
          console.log(data)
          reject(data)
        } else {
          let st = {}
          for (let d of data) {
            st[d.project_id] = {
              spent_time: d.minutes,
              subsystem: d.subsystem,
              estimation: d.estimation
            }
          }
          resolve(st)
        }
      })
    })
  ])
    .then(([projectL, spentTimeAndroid, spentTimeIOS, spentTimeOther]) => {
      let project = {}

      for (let pAnd of Object.keys(projectL)) {
        let p = {}

        if (spentTimeAndroid[pAnd]) {
          p = {
            name:
              projectL[pAnd].name +
              ' (' +
              spentTimeAndroid[pAnd].subsystem +
              ')',
            short_name: projectL[pAnd].short_name,
            spent_time: spentTimeAndroid[pAnd].spent_time,
            estimation: spentTimeAndroid[pAnd].estimation,
            type: spentTimeAndroid[pAnd].subsystem
          }
          project[p.name] = p
        }
        if (spentTimeIOS[pAnd]) {
          p = {
            name:
              projectL[pAnd].name + ' (' + spentTimeIOS[pAnd].subsystem + ')',
            short_name: projectL[pAnd].short_name,
            spent_time: spentTimeIOS[pAnd].spent_time,
            estimation: spentTimeIOS[pAnd].estimation,
            type: spentTimeIOS[pAnd].subsystem
          }
          project[p.name] = p
        }
        if (spentTimeOther[pAnd]) {
          p = {
            name:
              projectL[pAnd].name +
              ' (' +
              (spentTimeOther[pAnd].subsystem || 'Other') +
              ')',
            short_name: projectL[pAnd].short_name,
            spent_time: spentTimeOther[pAnd].spent_time,
            estimation: spentTimeOther[pAnd].estimation,
            type: spentTimeOther[pAnd].subsystem
          }
          project[p.name] = p
        }
      }
      // for (let pAnd of Object.keys(spentTimeAndroid)) {
      //   if (projectL[pAnd]) {
      //     let p = {
      //       name: projectL[pAnd] + ' (Android)',
      //       spent_time: spentTimeAndroid[pAnd].spent_time,
      //       estimation: spentTimeAndroid[pAnd].estimation
      //     }
      //     project[p.name] = p
      //   }
      // }
      // for (let pIos of Object.keys(spentTimeIOS)) {
      //   if (projectL[pIos]) {
      //     let p = {
      //       name: projectL[pIos] + ' (iOS)',
      //       spent_time: spentTimeIOS[pIos].spent_time,
      //       estimation: spentTimeIOS[pIos].estimation
      //     }
      //     project[p.name] = p
      //   }
      // }
      // for (let pOth of Object.keys(spentTimeOther)) {
      //   if (projectL[pOth]) {
      //     let p = {
      //       name: projectL[pOth] + ' (Other)',
      //       spent_time: spentTimeOther[pOth].spent_time,
      //       estimation: spentTimeOther[pOth].estimation
      //     }
      //     project[p.name] = p
      //   }
      // }
      res.send(project)
    })
    .catch(err => {
      res.status(500).send(err)
    })
})
app.post('/tasks_by_platform', (req, res) => {
  let filterValue = []
  if (req.body.period) {
    if (
      parseInt(req.body.period.value[0]) &&
      parseInt(req.body.period.value[1])
    ) {
      filterValue.push('t.created>=' + req.body.period.value[0])
      filterValue.push('t.created<=' + req.body.period.value[1])
    }
  }
  // res.status(200).json({ message: "I work" });
  Promise.all([
    new Promise(function (resolve, reject) {
      q = `select * from projects`
      db.table(q, [], function (err, data) {
        if (err) {
          console.log(111)
          console.log(data)
          reject(data)
        } else {
          let st = {}
          for (let d of data) {
            st[d.id] = {
              name: d.name,
              short_name: d.short_name
            }
          }
          resolve(st)
        }
      })
    }),
    new Promise(function (resolve, reject) {
      q = `
      select t.project_id,t.subsystem, count(tAll.task_id) as tatal_tasks,count(tCom.task_id) as tasks_complete from
      tasks as t
      left join tasks as tAll on tAll.task_id = t.task_id
      left join tasks as tCom on tCom.task_id = t.task_id and tCom.state in ('решена','закрыта')
      where lower(t.subsystem) = 'android' ${
        filterValue.length > 0 ? ' and ' + filterValue.join(' and ') : ''
      }
      group by t.project_id`
      db.table(q, [], function (err, data) {
        if (err) {
          console.log(111)
          console.log(data)
          reject(data)
        } else {
          let st = {}
          for (let d of data) {
            st[d.project_id] = {
              total: d.tatal_tasks,
              complete: d.tasks_complete,
              subsystem: d.subsystem
            }
          }
          resolve(st)
        }
      })
    }),
    new Promise(function (resolve, reject) {
      q = `select t.project_id,t.subsystem, count(tAll.task_id) as tatal_tasks,count(tCom.task_id) as tasks_complete from
      tasks as t
      left join tasks as tAll on tAll.task_id = t.task_id
      left join tasks as tCom on tCom.task_id = t.task_id and tCom.state in ('решена','закрыта')
      where lower(t.subsystem) = 'ios' ${
        filterValue.length > 0 ? ' and ' + filterValue.join(' and ') : ''
      }
      group by t.project_id
      `
      db.table(q, [], function (err, data) {
        if (err) {
          console.log(111)
          console.log(data)
          reject(data)
        } else {
          let st = {}
          for (let d of data) {
            st[d.project_id] = {
              total: d.tatal_tasks,
              complete: d.tasks_complete,
              subsystem: d.subsystem
            }
          }
          resolve(st)
        }
      })
    }),
    new Promise(function (resolve, reject) {
      q = `
      select t.project_id,t.subsystem, count(tAll.task_id) as tatal_tasks,count(tCom.task_id) as tasks_complete from
      tasks as t
      left join tasks as tAll on tAll.task_id = t.task_id
      left join tasks as tCom on tCom.task_id = t.task_id and tCom.state in ('решена','закрыта')
      where (lower(t.subsystem) != 'android' and lower(t.subsystem) != 'ios')  or t.subsystem ISNULL ${
        filterValue.length > 0 ? ' and ' + filterValue.join(' and ') : ''
      }
      group by t.project_id,t.subsystem
      `
      db.table(q, [], function (err, data) {
        if (err) {
          console.log(111)
          console.log(data)
          reject(data)
        } else {
          let st = {}
          for (let d of data) {
            st[d.project_id] = {
              total: d.tatal_tasks,
              complete: d.tasks_complete,
              subsystem: d.subsystem
            }
          }
          resolve(st)
        }
      })
    })
  ])
    .then(([projectL, spentTimeAndroid, spentTimeIOS, spentTimeOther]) => {
      let project = {}

      for (let pAnd of Object.keys(projectL)) {
        let p = {}

        if (spentTimeAndroid[pAnd]) {
          p = {
            name:
              projectL[pAnd].name +
              ' (' +
              spentTimeAndroid[pAnd].subsystem +
              ')',
            short_name: projectL[pAnd].short_name,
            total: spentTimeAndroid[pAnd].total,
            complete: spentTimeAndroid[pAnd].complete,
            type: spentTimeAndroid[pAnd].subsystem
          }

          project[p.name] = p
        }
        if (spentTimeIOS[pAnd]) {
          p = {
            name:
              projectL[pAnd].name + ' (' + spentTimeIOS[pAnd].subsystem + ')',
            short_name: projectL[pAnd].short_name,
            total: spentTimeIOS[pAnd].total,
            complete: spentTimeIOS[pAnd].complete,
            type: spentTimeIOS[pAnd].subsystem
          }
          project[p.name] = p
        }
        if (spentTimeOther[pAnd]) {
          p = {
            name:
              projectL[pAnd].name +
              ' (' +
              (spentTimeOther[pAnd].subsystem || 'Other') +
              ')',
            short_name: projectL[pAnd].short_name,
            total: spentTimeOther[pAnd].total,
            complete: spentTimeOther[pAnd].complete,
            type: spentTimeOther[pAnd].subsystem
          }
          project[p.name] = p
        }
      }
      res.send(project)
    })
    .catch(err => {
      res.status(500).send(err)
    })
})

module.exports = {
  path: '/api',
  handler: app
}
