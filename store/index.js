import Vue from 'vue'
import Vuex from 'vuex'
import axios from 'axios'
// import VueAxios from 'vue-axios'
// import cookies from 'cookie-universal-nuxt'

if (process.client) {
  // Vue.use(VueAxios, axios)
}

Vue.use(Vuex)
const store = () =>
  new Vuex.Store({
    state: {
     //apiLink: 'http://94.127.67.113'
      // apiLink: 'http://23.111.121.119'
      apiLink: 'http://localhost:3000'
    },
    mutations: {},
    actions: {
      login (state, { email, password }) {},
      async nuxtServerInit ({ commit, state }, { app }) {
        app.$axios.onRequest(request => {
          request.url = state.apiLink + request.url

          return request
        })
        // if (app.$cookies.get('token')) {
        //   app.$axios.setToken(app.$cookies.get('token'), 'Bearer')
        // }
      },
      chSession (state, {}) {}
    },
    getters: {
      user (state) {
        return state.user
      },
      apiLink (state) {
        return state.apiLink
      }
    }
  })

export default store
